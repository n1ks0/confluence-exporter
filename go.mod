module pulse.insure/confluence-exporter

go 1.22

require (
	github.com/sedmess/go-ctx v0.9.20
	github.com/virtomize/confluence-go-api v1.4.6
)

require github.com/magefile/mage v1.14.0 // indirect
