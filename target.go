package main

import (
	"github.com/sedmess/go-ctx/logger"
	goconfluence "github.com/virtomize/confluence-go-api"
)

type ConfluenceClient struct {
	l logger.Logger `logger:""`

	baseUrl  string `env:"BASE_URL"`
	username string `env:"USERNAME"`
	password string `env:"PASSWORD"`

	spaceId      int    `env:"SPACE_ID"`
	parentPageId string `env:"PARENT_PAGE_ID"`

	api *goconfluence.API
}

func (c *ConfluenceClient) Init() {
	api, err := goconfluence.NewAPI(c.baseUrl, c.username, c.password)
	if err != nil {
		c.l.Fatal("can't create confluence client:", err)
	}
	c.api = api
}

func (c *ConfluenceClient) FindAll() Page {
	content, err := c.api.GetContentByID(c.parentPageId, goconfluence.ContentQuery{})
	if err != nil {
		c.l.Fatal("can't get parent content:", err)
	}
	return Page{Id: c.parentPageId, Title: content.Title, Children: c.findChildren(c.parentPageId)}
}

func (c *ConfluenceClient) RemovePage(pageId string) {
	_, err := c.api.DelContent(pageId)
	if err != nil {
		c.l.Fatal("can't remove page", pageId, ":", err)
	}
}

func (c *ConfluenceClient) CreatePage(parentPageId string, title string, content string) string {
	request := goconfluence.Content{
		Title:     title,
		Status:    "current",
		Type:      "page",
		Space:     &goconfluence.Space{ID: c.spaceId},
		Ancestors: []goconfluence.Ancestor{{ID: parentPageId}},
		Body: goconfluence.Body{
			Storage: goconfluence.Storage{Value: content, Representation: "storage"},
		},
	}
	createdContent, err := c.api.CreateContent(&request)
	if err != nil {
		c.l.Fatal("can't create page", title, ":", err)
	}
	return createdContent.ID
}

func (c *ConfluenceClient) UpdatePage(pageId string, content string) {
	currentContent, err := c.api.GetContentByID(pageId, goconfluence.ContentQuery{Expand: []string{"version"}})
	if err != nil {
		c.l.Fatal("can't get current content of", pageId, ":", err)
	}

	request := goconfluence.Content{
		ID:     pageId,
		Status: "current",
		Type:   "page",
		Title:  currentContent.Title,
		Body: goconfluence.Body{
			Storage: goconfluence.Storage{Value: content, Representation: "storage"},
		},
		Version: &goconfluence.Version{
			Number:  currentContent.Version.Number + 1,
			Message: "autoupdate",
		},
	}
	_, err = c.api.UpdateContent(&request)
	if err != nil {
		c.l.Fatal("can't update page", pageId, ":", err)
	}
}

func (c *ConfluenceClient) findChildren(id string) map[string]Page {
	search, err := c.api.GetChildPages(id)
	if err != nil {
		c.l.Fatal("can't get pages:", err)
	}
	pages := map[string]Page{}
	for _, result := range search.Results {
		if result.Type != "page" || result.Status != "current" {
			continue
		}
		c.l.Info("found page:", result.Title)
		pages[result.Title] = Page{Title: result.Title, Id: result.ID, Children: c.findChildren(result.ID)}
	}
	return pages
}
