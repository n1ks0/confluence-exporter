package main

import (
	"bytes"
	"github.com/sedmess/go-ctx/logger"
	"strings"
	"text/template"
)

type PageTemplate struct {
	l logger.Logger `logger:""`

	templateString string `env:"PAGE_TEMPLATE"`
	escapeXml      bool   `env:"PAGE_TEMPLATE_XML_CDATA" envDef:"false"`
	template       *template.Template
	contentEscaper func(str string) string
}

func (t *PageTemplate) Init() {
	templ, err := template.New("page").Parse(t.templateString)
	if err != nil {
		t.l.Fatal("can't parse page template:", err)
	}
	t.template = templ

	if t.escapeXml {
		t.contentEscaper = func(str string) string {
			return "<![CDATA[" + strings.ReplaceAll(str, "]]>", "<![CDATA[]]]]><![CDATA[>]]>") + "]]>" // fixme
		}
	} else {
		t.contentEscaper = func(str string) string {
			return str
		}
	}
}

func (t *PageTemplate) Apply(content string) string {
	buffer := bytes.NewBuffer(make([]byte, 0))
	data := struct {
		Content string
	}{
		Content: t.contentEscaper(content),
	}
	err := t.template.Execute(buffer, data)
	if err != nil {
		t.l.Fatal("can't prepare page:", err)
	}
	result := buffer.String()
	return result
}
