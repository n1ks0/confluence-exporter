package main

import (
	"github.com/sedmess/go-ctx/logger"
	"os"
)

var obIsDoneCh = make(chan bool)

type Job struct {
	l logger.Logger `logger:""`

	source           *SourceProvider   `inject:""`
	confluenceClient *ConfluenceClient `inject:""`
	pageTemplate     *PageTemplate     `inject:""`

	done chan any
}

func (j *Job) Init() {
	j.done = make(chan any)
}

func (j *Job) AfterStart() {
	go j.do()
}

func (j *Job) BeforeStop() {}

func (j *Job) Done() <-chan any {
	return j.done
}

func (j *Job) do() {
	defer func() { j.done <- true }()

	j.l.Info("=== start ===")

	sourcePageCh := Async(j.source.FindAll)
	targetPageCh := Async(j.confluenceClient.FindAll)

	sourcePage := <-sourcePageCh
	targetPage := <-targetPageCh

	sourcePage.Id = targetPage.Id

	j.l.Info("=== pages collected ===")

	var pagesToRemove []Page
	var targetPagesWalker func(source Page, target Page)
	targetPagesWalker = func(source Page, target Page) {
		for key, tPage := range target.Children {
			if sPage, ok := source.Children[key]; ok {
				sPage.Id = tPage.Id
				source.Children[key] = sPage
				targetPagesWalker(sPage, tPage)
			} else {
				pagesToRemove = append(pagesToRemove, tPage)
			}
		}
	}
	targetPagesWalker(sourcePage, targetPage)

	j.l.Info("=== found to delete:", len(pagesToRemove), "===")

	var pageRemover func(page Page)
	pageRemover = func(page Page) {
		for _, page := range page.Children {
			j.l.Info("removing page:", page.Id)
			pageRemover(page)
		}
		j.l.Info("removing page", page.Id)
		j.confluenceClient.RemovePage(page.Id)
	}
	for _, page := range pagesToRemove {
		pageRemover(page)
	}

	j.l.Info("=== upserting ===")

	var pageUpserter func(page Page)
	pageUpserter = func(parentPage Page) {
		for _, page := range parentPage.Children {
			if page.Id == "" {
				if len(page.Children) > 0 {
					j.l.Info("create blank page", page.Title)
					pageId := j.confluenceClient.CreatePage(parentPage.Id, page.Title, "")
					page.Id = pageId
					pageUpserter(page)
				} else {
					j.l.Info("create content page", page.Title)
					contentBytes, err := os.ReadFile(page.SourcePath)
					if err != nil {
						j.l.Fatal("can't read file", page.SourcePath, ":", err)
					}
					_ = j.confluenceClient.CreatePage(parentPage.Id, page.Title, j.pageTemplate.Apply(string(contentBytes)))
				}
			} else {
				if len(page.Children) > 0 {
					pageUpserter(page)
				} else {
					j.l.Info("update content page", page.Id, page.Title)
					contentBytes, err := os.ReadFile(page.SourcePath)
					if err != nil {
						j.l.Fatal("can't read file", page.SourcePath, ":", err)
					}
					j.confluenceClient.UpdatePage(page.Id, j.pageTemplate.Apply(string(contentBytes)))
				}
			}
		}
	}
	pageUpserter(sourcePage)

	j.l.Info("=== done ===")
}
