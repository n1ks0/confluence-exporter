package main

type Page struct {
	Id         string
	SpaceId    string
	Title      string
	SourcePath string
	Children   map[string]Page
}
