package main

import (
	"github.com/sedmess/go-ctx/ctx"
)

func main() {
	job := &Job{}
	application := ctx.CreateContextualizedApplication(
		ctx.PackageOf(&SourceProvider{}, &ConfluenceClient{}, &PageTemplate{}, job),
	)
	<-job.Done()
	application.Stop().Join()
}
