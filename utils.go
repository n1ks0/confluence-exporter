package main

func Async[T any](fn func() T) <-chan T {
	ch := make(chan T)
	go func() {
		defer close(ch)
		ch <- fn()
	}()
	return ch
}

func First[T any](first T, _ ...any) T {
	return first
}
