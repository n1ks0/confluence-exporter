package main

import (
	"github.com/sedmess/go-ctx/logger"
	"os"
	"path/filepath"
	"strings"
)

type SourceProvider struct {
	l logger.Logger `logger:""`

	path      string          `env:"SOURCE_PATH" envDef:"./"`
	extFilter map[string]bool `env:"SOURCE_EXT_FILTER" envDef:".puml"`
}

func (f *SourceProvider) FindAll() Page {
	return f.findAll(f.path)
}

func (f *SourceProvider) findAll(path string) Page {
	entries, err := os.ReadDir(path)
	if err != nil {
		f.l.Fatal("error reading dir", err)
	}
	pages := map[string]Page{}
	for _, entry := range entries {
		filePath := filepath.Join(path, entry.Name())
		fileExt := filepath.Ext(entry.Name())
		if entry.IsDir() {
			page := f.findAll(filePath)
			if len(page.Children) > 0 {
				pages[entry.Name()] = page
			}
		} else if f.extFilter[fileExt] {
			f.l.Info("found", filePath)
			fileName := First(strings.CutSuffix(entry.Name(), fileExt))
			pages[fileName] = Page{SourcePath: filePath, Title: fileName}
		} else {
			f.l.Info("skipping", entry.Name())
		}
	}
	return Page{Title: filepath.Base(path), Children: pages}
}
