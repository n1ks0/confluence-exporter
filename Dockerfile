FROM golang:1.22 as builder

WORKDIR /build

COPY ./go.* ./
RUN go mod download
COPY . .
RUN mkdir -p bin && go test -v ./... && CGO_ENABLED=0 go build -buildvcs=false -v -ldflags="-s -w" -o ./bin/exporter ./

FROM gcr.io/distroless/static-debian12:nonroot

WORKDIR /app
COPY --from=builder /build/bin/exporter /app/exporter

ENTRYPOINT ["/app/exporter"]